from django_tables2.utils import A  # alias for Accessor

class MyItemsTable(tables.Table):
    id = tables.LinkColumn('repayment', args=[A('pk')])
    class Meta:
        model = models.Loan