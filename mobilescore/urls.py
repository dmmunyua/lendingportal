from django.conf.urls import url
from . import views
urlpatterns = [
    url(r'^$',views.index,name='index'),
    url(r'^flot/', views.flot, name='flot'),
    url(r'^disbursement/', views.disbursement, name='disbursement'),
    url(r'loan/(?P<pk>\d+)/', views.repayment, name='repayment'),
    url(r'^active', views.active, name='active'),
    url(r'^pending', views.pending, name='pending'),
    url(r'^verified', views.verified, name='verified'),
    url(r'^rejected', views.rejected, name='rejected'),
    url(r'^loan/', views.loan, name='loan'),
]