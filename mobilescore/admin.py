# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from mobilescore.models import Customer, MobileScore, Score, Loan, UserProfile
from partner.models import Partner, PartnerScore, PartnerUserProfile
# Register your models here.

class CustomerAdmin(admin.ModelAdmin):
                list_display = (
                	'QCustomerID',
                	'MSISDN',
                	'firstName',
                	'lastName',
                	'documentType',
                	'documentNumber',
                	'DOB',
                	'status',
                	'consent',
                	'registrationDate',
                	'alias',
                	'pinStatus'
                	)

class MobileScoreAdmin(admin.ModelAdmin):
				list_display = (
					'customer',
					'documentType',
					'documentNumber',
					'firstName',
					'middleName',
					'lastName',
					'DOB',
					'existence',
					'averageValueIn',
					'averageValueOut',
					'overdue',
					'default',
					'averagePayments',
					'maxValueReceived',
					'maxValueSent'
					)

class ScoreAdmin(admin.ModelAdmin):
				list_display = (
					'MSISDN',
					'customer',
					'score',
					'scoreDate'
					)

class LoanAdmin(admin.ModelAdmin):
				list_display = (
					'customer',
					'requestedDate',
					'requestedAmount',
					'disbused',
					'disbursedReceipt',
					'disbusedAmount',
					'status',
					'dueDate',
					'rollOverDate',
					'expiryDate',
					'loanBalance',
					'overpaid',
					'rollOverPenaltyAmount',
					'overduePenaltyAmount',
					'paidDate'
					)

class UserProfileAdmin(admin.ModelAdmin):
				list_display = (
					'user',
					'firstName',
					'lastName',
					'documentType',
					'documentNumber',
					'DOB',
					'MSISDN',
					'status',
					'registrationDate'
					)


class PartnerAdmin(admin.ModelAdmin):
				list_display = (
					'partnerName',
					'KRAPIN',
					'registrationNumber',
					'physicalAddress',
					'postalAddress',
					'registrationDate',
					'status',
					'partnerInternalID'
					)

class PartnerScoreAdmin(admin.ModelAdmin):
				list_display = (
					'partner',
					'firstName',
					'middleName',
					'lastName',
					'DOB',
					'documentType',
					'documentNumber',
					'existence',
					'loanID',
					'amount',
					'status',
					'event',
					'loanTerm',
					'loanDate',
					'repaymentDate'
					)

class PartnerUserProfileAdmin(admin.ModelAdmin):
				list_display = (
					'user',
					'partner',
					'firstName',
					'lastName',
					'documentType',
					'documentNumber',
					'DOB',
					'MSISDN',
					'status',
					'registrationDate'
					)


admin.site.register(Customer, CustomerAdmin)
admin.site.register(MobileScore, MobileScoreAdmin)
admin.site.register(Score, ScoreAdmin)
admin.site.register(Loan, LoanAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Partner, PartnerAdmin)
admin.site.register(PartnerScore, PartnerScoreAdmin)
admin.site.register(PartnerUserProfile, PartnerUserProfileAdmin)

