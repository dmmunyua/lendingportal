# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User #added line to support User
from django.urls import reverse


# Create your models here.

class Customer(models.Model):
	DOCUMENT_TYPE_OPTIONS = (
		('nationalid', 'NATIONALID'),
		('passport', 'PASSPORT'),
		)
	STATUS_OPTIONS = (
		('new','NEW'),
		('active','ACTIVE'),
		('pendingkyc','PENDINGKYC'),
		('blacklisted','BLACKLISTED'),
		('closed','CLOSED'),
		('suspended','SUSPENDED'),
		)
	PIN_OPTIONS = (
		('active','ACTIVE'),
		('pendingchange','PENDINGCHANGE'),
		('locked','LOCKED'),
		)
	firstName = models.CharField(max_length=255)
	lastName = models.CharField(max_length=255)
	documentType = models.CharField(max_length=11, choices=DOCUMENT_TYPE_OPTIONS, default='nationalid')
	documentNumber = models.CharField(max_length=8)
	DOB = models.DateField(null=True, blank=True)
	MSISDN = models.BigIntegerField()
	QCustomerID = models.CharField(max_length=255, unique=True)
	status =  models.CharField(max_length=11, choices=STATUS_OPTIONS, default='new')
	consent = models.BooleanField(default=False)
	registrationDate = models.DateTimeField(default=timezone.now)
	alias = JSONField(blank=True, null=True)
	PIN = models.IntegerField()
	pinStatus = models.CharField(max_length=13, choices=PIN_OPTIONS, default='active')

	def __unicode__(self):
		return str(self.QCustomerID)

class MobileScore(models.Model):
	customer = models.ForeignKey("Customer", on_delete=models.SET_NULL,
		blank=True,
		null=True,
		)
	existence = models.CharField(max_length=255, null=True, blank=True)
	averageValueIn = models.CharField(max_length=255, null=True, blank=True)
	averageValueOut = models.CharField(max_length=255, null=True, blank=True)
	overdue = models.CharField(max_length=255, null=True, blank=True)
	default = models.CharField(max_length=255, null=True, blank=True)
	averagePayments = models.CharField(max_length=255, null=True, blank=True)
	firstName =  models.CharField(max_length=255, null=True, blank=True)
	middleName = models.CharField(max_length=255, null=True, blank=True)
	lastName = models.CharField(max_length=255, null=True, blank=True)
	maxValueReceived = models.CharField(max_length=255, null=True, blank=True)
	maxValueSent = models.CharField(max_length=255, null=True, blank=True)
	documentType = models.CharField(max_length=255, null=True, blank=True)
	documentNumber = models.CharField(max_length=255, null=True, blank=True)
	DOB = models.CharField(max_length=255, null=True, blank=True)

	def __unicode__(self):
		return str(self.id)


class Score(models.Model):
	MSISDN = models.BigIntegerField()
	customer = models.ForeignKey("Customer", on_delete=models.SET_NULL,
		blank=True,
		null=True,
		)
	score = JSONField()
	scoreDate = models.DateTimeField()

	def __unicode__(self):
		return str(self.MSISDN)


class Loan(models.Model):
	STATUS_OPTIONS = (
		('active','ACTIVE'),
		('paid','PAID'),
		('overpaid','OVERPAID'),
		('rolledover','ROLLEDOVER'),
		('default','DEFAULT'),
		)
	customer = models.ForeignKey("Customer", on_delete=models.SET_NULL,
		blank=True,
		null=True,
		)
	requestedDate = models.DateTimeField()
	requestedAmount = models.IntegerField()
	disbused = models.BooleanField(default=False)
	disbursedReceipt = models.CharField(max_length=255, null=True, blank=True)
	disbursedDate = models.DateTimeField(null=True, blank=True)
	disbusedAmount = models.IntegerField(default=0)
	status = models.CharField(max_length=10, choices=STATUS_OPTIONS, default='active')
	dueDate = models.DateTimeField()
	rollOverDate = models.DateTimeField(null=True, blank=True)
	expiryDate = models.DateTimeField(null=True, blank=True)
	loanBalance = models.IntegerField()
	overpaid = models.BooleanField(default=False)
	rollOverPenaltyAmount = models.IntegerField(default=0)
	overduePenaltyAmount = models.IntegerField(default=0)
	paidDate = models.DateTimeField(null=True, blank=True)

	"""def get_absolute_url(self):
		return reverse('repayment', kwargs={'pk': self.id})"""

	def __unicode__(self):
		return str(self.id)


class UserProfile(models.Model):
	DOCUMENT_TYPE_OPTIONS = (
		('nationalid', 'NATIONALID'),
		('passport', 'PASSPORT'),
		)
	USER_STATUS = (
		('active','ACTIVE'),
		('suspended','SUSPENDED'),
		('closed','CLOSED'),
		)
	firstName = models.CharField(max_length=255)
	lastName = models.CharField(max_length=255)
	documentType = models.CharField(max_length=11, choices=DOCUMENT_TYPE_OPTIONS, default='nationalid')
	documentNumber = models.CharField(max_length=8)
	DOB = models.DateField(null=True, blank=True)
	MSISDN = models.BigIntegerField()
	user = models.OneToOneField(User, unique=True)
	status = models.CharField(max_length=10, choices=USER_STATUS, default='active')
	registrationDate = models.DateField(null=True, blank=True)

	def __unicode__(self):
	    return str(self.user)





















