# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, redirect, get_object_or_404
from mobilescore.models import Customer, Loan
from django.views.generic import DetailView
# Create your views here.


# Create your views here.
def index(request):
	if request.user.is_authenticated:
		return render(request, 'base.html')
	else:
		return redirect('/accounts/login/')

def flot(request):
	return render(request, 'flot.html')

def disbursement(request):
	if request.user.is_authenticated:
		disbursements = Loan.objects.all()
		return render(request, 'datatables.html', {'disbursements':disbursements})
	else:
		return redirect('/accounts/login/')


def repayment(request, pk):
	if request.user.is_authenticated:
		loan = get_object_or_404(Loan, pk=pk)
		return render(request, 'loan_detail.html', {'loan': loan})
	else:
		return redirect('/accounts/login')


def loan(request):
	if request.user.is_authenticated:
		context = {
			'loans': Loan.objects.all()
		}
		return render(request, 'loans.html', context)
	else:
		return redirect('/accounts/login')


def active(request):
	if request.user.is_authenticated:
		customers = Customer.objects.filter(status="new")
		# print(customers)
		return render(request, 'active.html', {'customers':customers})
	else:
		return redirect('/accounts/login')


def pending(request):
	if request.user.is_authenticated:
		customers = Customer.objects.filter(status="pendingkyc")
		return render(request, 'pending.html', {'customers':customers})
	else:
		return redirect('/accounts/login')


def verified(request):
	if request.user.is_authenticated:
		customers = Customer.objects.filter(status="active")
		return render(request, 'verified.html', {'customers':customers})
	else:
		return redirect('/accounts/login')


def rejected(request):
	if request.user.is_authenticated:
		customers = Customer.objects.filter(status="blacklisted")
		return render(request, 'rejected.html', {'customers':customers})
	else:
		return redirect('/accounts/login')
