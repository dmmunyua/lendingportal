"""
A custom backend that aims to include first_name and last_name
during new user registration.
It requires a custom form with custom fields, in our example: first_name and
last_name
"""

from registration.backends.simple.views import RegistrationView
# from registration.backends.default.views import RegistrationView
from mobilescore.forms import RegForm


class RegBackend(RegistrationView):
    form_class = RegForm
    success_url = '/mobilescore/'   # For default backend use url for 'register_complete'

    def register(self, form_class):
        new_user = super(RegBackend, self).register(form_class)
        first_name = form_class.cleaned_data['first_name']
        last_name = form_class.cleaned_data['last_name']
        new_user.first_name = first_name
        new_user.last_name = last_name
        new_user.save()

        return new_user
