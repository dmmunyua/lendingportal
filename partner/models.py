# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User #added line to support User

# Create your models here.

class Partner(models.Model):
	PARTNER_STATUS = (
		('active','ACTIVE'),
		('inactive','INACTIVE'),
		('closed','CLOSED'),
		('suspended','SUSPENDED'),
		)
	partnerName = models.CharField(max_length=255, unique=True)
	KRAPIN = models.CharField(max_length=10)
	registrationNumber = models.CharField(max_length=10)
	contact = JSONField()
	physicalAddress = models.CharField(max_length=255)
	postalAddress = models.CharField(max_length=255)
	registrationDate = models.DateTimeField()
	status = models.CharField(max_length=9, choices=PARTNER_STATUS, default ='active')
	partnerInternalID = models.CharField(max_length=255, unique=True)

	def __unicode__(self):
		return str(self.partnerName)


class PartnerScore(models.Model):
	DOCUMENT_TYPE_OPTIONS = (
		('nationalid', 'NATIONALID'),
		('passport', 'PASSPORT'),
		)
	STATUS_OPTIONS = (
		('active','ACTIVE'),
		('paid','PAID'),
		('overpaid','OVERPAID'),
		('rolledover','ROLLEDOVER'),
		('default','DEFAULT'),
		)
	EVENT_OPTIONS = (
		('repayment','REPAYMENT'),
		('disbursement','DISBURSEMENT'),
		('loanstatus','LOANSTATUS'),
		)
	existence = models.CharField(max_length=255, blank=True, null=True)
	amount = models.CharField(max_length=255)
	firstName = models.CharField(max_length=255)
	lastName = models.CharField(max_length=255)
	middleName = models.CharField(max_length=255, blank=True, null=True)
	documentType = models.CharField(max_length=11, choices=DOCUMENT_TYPE_OPTIONS, default='nationalid')
	documentNumber = models.CharField(max_length=8)
	DOB = models.DateField(null=True, blank=True)
	partner = models.ForeignKey("Partner", on_delete=models.SET_NULL,
		blank=True,
		null=True,
		)
	status = models.CharField(max_length=10, choices=STATUS_OPTIONS, default='active')
	loanID = models.CharField(max_length=255, unique =True)
	event = models.CharField(max_length=12, choices=EVENT_OPTIONS, default='loanstatus')
	loanTerm = models.CharField(max_length=2)
	loanDate = models.DateField()
	repaymentDate = models.DateField(null=True, blank=True)

	def __unicode__(self):
		return str(self.loanID)

class PartnerUserProfile(models.Model):
	DOCUMENT_TYPE_OPTIONS = (
		('nationalid', 'NATIONALID'),
		('passport', 'PASSPORT'),
		)
	USER_STATUS = (
		('active','ACTIVE'),
		('suspended','SUSPENDED'),
		('closed','CLOSED'),
		)
	firstName = models.CharField(max_length=255)
	lastName = models.CharField(max_length=255)
	documentType = models.CharField(max_length=11, choices=DOCUMENT_TYPE_OPTIONS, default='nationalid')
	documentNumber = models.CharField(max_length=8)
	DOB = models.DateField(null=True, blank=True)
	MSISDN = models.BigIntegerField()
	partner = models.ForeignKey("Partner")
	user = models.OneToOneField(User, unique=True)
	status = models.CharField(max_length=10, choices=USER_STATUS, default='active')
	registrationDate = models.DateField(null=True, blank=True)

	def __unicode__(self):
	    return str(self.user)	











