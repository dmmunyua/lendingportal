Django==1.11.6
django-bootstrap-themes==3.3.6
django-bootstrap3==9.1.0
django-crispy-forms==1.7.0
django-registration-redux==1.9
django-widget-tweaks==1.4.1
psycopg2==2.7.3.2
python-decouple==3.1
pytz==2017.3
